<?php


use Illuminate\Support\Facades\Route;

//especificar que voy a usar el controlador
use App\Http\Controllers\ScrapingController;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/scraping',[ScrapingController::class,'example']);

Route::get('/bitacora',[ScrapingController::class,'bitacora']);

Route::get('/fecha',[ScrapingController::class,'fecha']);

<?php

namespace App\Console\Commands;

use App\Models\Dollar;
use Goutte\Client;
use Illuminate\Console\Command;

class Consultadia extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'Consulta:dia';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $instancia = new Client();
        $crawler = $instancia->request('GET', 'https://www.banxico.org.mx/tipcamb/llenarTiposCambioAction.do?idioma=sp');//direccion a la que vamos a acceder
        $buscador = '#tdSF43718';//el filtro de búsqueda
        //$mostrar = $crawler->filter("#tdSF43718")->first();
        $mostrar = $crawler->filter("$buscador")->first()->text();
        $precio = $mostrar;


        //guardar en la bd
        $consulta = new Dollar(); //Creamos la instancia de la clase Articulo
        $consulta->precio = $precio;

        $consulta->save();
    }
}

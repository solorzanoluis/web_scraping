<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use App\Http\Controllers\ScrapingController;


class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */

    protected $commands=[
        Commands\Consulta::class
    ];


    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('Consulta:consultar')->everyMinute();
        $schedule->command('Consulta:consultar')->dailyAt('08:00');//ejecutarlo a diario a las 8:00am
       
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

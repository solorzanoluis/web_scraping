<?php

namespace App\Http\Controllers;

use App\Models\Dollar;
use DateTime;
use Goutte\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpClient\HttpClient;

class ScrapingController extends Controller
{
    public function example(){
        
        $cr = new Client(HttpClient::create(['timeout' => 300]));//Hacemos la consulta cada 5 minutos(300 segundos)
        $crawler = $cr->request('GET', 'https://www.banxico.org.mx/tipcamb/llenarTiposCambioAction.do?idioma=sp');//direccion a la que vamos a acceder
        $buscador = '#tdSF43718';//el filtro de búsqueda
        $mostrar = $crawler->filter("$buscador")->first()->text();
        //echo (gettype($mostrar));
        //dd($mostrar);

        //$precio = $mostrar;

        $hoy = date('Y-m-d');
        
        $hora = date('H:i:s');
        
        //guardar en la bd
        // $consulta = new Dollar(); //Creamos la instancia de la clase Articulo
        // $consulta->precio = $precio;
        // $consulta->fecha = $hoy;
        // $consulta->save();

        //$precios = Dollar::all();
        $precios = Dollar::all()->where('fecha',$hoy);

        return view('datos')->with('precio',$precios);


    }

    public function bitacora(){

        //$precios = Dollar::all()->sortByDesc('fecha');
        $precios = Dollar::all();
        $precios = Dollar::orderby('fecha','Desc')->get();

        return view('bitacora')->with('precio',$precios);
    }

    public function fecha(){
        $dt = new DateTime();
        echo $dt->format('Y-m-d H:i:s');
    }
}

@extends('layouts.plantilla');

@section('title','Precio Hoy')

@section('contenido')

    <div class="container">

        <h3 class="bg-primary text-white text-center mt-5" style="padding:10px">Valor del dollar el día de hoy: {{ date('Y-m-d') }}</h3> 


        <a href="/bitacora" class="btn btn-primary text-bold text-white">Visualizar Bitácora</a>

        <table id="articulos" class="table table-striped table-bordered shadow-lg mt-4" style="width:100%">
            <thead class="bg-primary text-white">
                {{-- <th scope="col">No</th> --}}
                <th scope="col">Precio</th>
                <th scope="col">Fecha</th>
                {{-- <th scope="col">Horario</th> --}}
            </thead>
    
            <tbody>
                @foreach ($precio as $p=>$dato)
                    <tr>
                        {{-- <td>{{++$p}}</td> --}}
                        <td>{{$dato->precio}}</td>
                        <td>{{$dato->created_at}}</td>
                        {{-- <td>{{$dato->hora}}</td> --}}
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>

    {{-- <script>
        function redireccionar() {
            setTimeout('document.location.reload(/datos.blade.php)',10000);
        }
    </script> --}}

    {{-- <script type="text/javascript">
        function actualizar(){location.reload(true);}
        //Función para actualizar cada 5 segundos(5000 milisegundos)
        setInterval("actualizar()",5000);
    </script> --}}

@endsection
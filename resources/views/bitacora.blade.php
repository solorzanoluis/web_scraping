@extends('layouts.plantilla');

@section('title','Bitácora')

@section('contenido')

<div class="container">

    <h3 class="bg-secondary text-white text-center mt-5" style="padding: 10px">Bitácora de consultas</h3>

    <a href="/scraping" class="btn btn-primary text-bold text-white">Regresar</a>

    <table id="articulos" class="table table-striped table-bordered shadow-lg mt-4" style="width:100%">
        <thead class="bg-primary text-white">
            <th scope="col">No</th>
            <th scope="col">Precio</th>
            <th scope="col">Fecha de consulta</th>
        </thead>

        <tbody>
            @foreach ($precio as $p=>$dato)
                <tr>
                    <td>{{++$p}}</td>
                    <td>{{$dato->precio}}</td>
                    <td>{{$dato->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

</div>
    
@endsection